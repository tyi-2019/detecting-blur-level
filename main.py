import cv2 as cv
import numpy as np

import glob

imgfiles = []
for file in glob.glob("./images/*"):
    imgfiles.append(file)

# print(imgfiles)
def fun(imgfiles):
    Max = 0
    show = []
    
    for imgway in imgfiles:
        img = cv.imread(imgway)
        raz = 0
        for i in range(0, img.shape[0]):
            for j in range(0, img.shape[1] - 1):
                raz+= abs(int(img[i][j][0]) - int(img[i][j + 1][0]) + int(img[i][j][1]) - int(img[i][j + 1][1]) + int(img[i][j][2]) - int(img[i][j + 1][2]))
        show.append((img, raz))
        print(raz)

    def sortSecond(val):
        return val[1]

show.sort(key=sortSecond, reverse=True)

return show


def getR(img):
    raz = 0
    for i in range(0, img.shape[0]):
        for j in range(0, img.shape[1] - 1):
            raz+= abs(int(img[i][j][0]) - int(img[i][j + 1][0]) + int(img[i][j][1]) - int(img[i][j + 1][1]) + int(img[i][j][2]) - int(img[i][j + 1][2]))
    return raz

print(getR(cv.imread(imgfiles[0])))

show = fun(imgfiles)
cv.imshow('Image', show[0][0])

cv.waitKey(0)
cv.destroyAllWindows()
